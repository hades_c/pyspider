FROM python:3.6-alpine3.15

RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g' /etc/apk/repositories

RUN apk update \
    && apk upgrade \
    && apk add --no-cache bash git vim gcc make g++\
    bash-doc \
    bash-completion

# install phantomjs
RUN mkdir -p /opt/phantomjs \
        && cd /opt/phantomjs
# COPY phantomjs-2.1.1.tar.gz /opt/phantomjs/phantomjs-2.1.1.tar.gz

COPY phantomjs.2.1.tar.gz /opt/phantomjs/phantomjs.2.1.tar.gz

RUN cd /opt/phantomjs \
        # && wget -O phantomjs.tar.gz https://github.com/ariya/phantomjs/archive/refs/tags/2.1.1.tar.gz \
        && tar xavf phantomjs.2.1.tar.gz --strip-components 1 \
        && python build.py -d \
        # && ln -s /opt/phantomjs /usr/local/bin/phantomjs \
        && rm phantomjs.2.1.tar.gz
# Fix Error: libssl_conf.so: cannot open shared object file: No such file or directory
ENV OPENSSL_CONF=/etc/ssl/

# install nodejs
ENV NODEJS_VERSION=8.15.0 \
    PATH=$PATH:/opt/node/bin
WORKDIR "/opt/node"

RUN apk add --no-cache curl  chromium libxml2 libxslt libxml2-dev
RUN apk add --no-cache nodejs
RUN apk add --no-cache npm


    # curl -sL https://nodejs.org/dist/v${NODEJS_VERSION}/node-v${NODEJS_VERSION}-linux-x64.tar.gz | tar xz --strip-components=1 && \
    # rm -rf /var/lib/apt/lists/*

RUN export PUPPETEER_SKIP_DOWNLOAD='true'

RUN npm install puppeteer express

# install requirements
COPY requirements.txt /opt/pyspider/requirements.txt 
RUN pip install -r /opt/pyspider/requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple/

RUN npm cache clean

# add all repo
ADD ./ /opt/pyspider

# run test
WORKDIR /opt/pyspider
RUN pip install -e .[all]

# Create a symbolic link to node_modules
RUN ln -s /opt/node/node_modules ./node_modules

#VOLUME ["/opt/pyspider"]
ENTRYPOINT ["pyspider"]

EXPOSE 5000 23333 24444 25555 22222