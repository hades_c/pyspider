<!--
 * @Author: Chiron
 * @Date: 2023-02-02 20:21:07
 * @LastEditTime: 2023-03-03 17:25:55
 * @LastEditors: Chiron
 * @Description: 
-->


# 生成docker 镜像
```
docker build  --force-rm -f Dockerfile.slim -t chiron/pyspider:amd64 .
```

```
docker-compose -f docker-compose1.yaml up
```

# docker 启动合集
```
docker run --restart=always -it -d  --name scrapydweb -p 5001:5000 -v /Users/chiron/Data/docker_volumes/scrapy/web_projects:/scrapydweb chiron/scrapydweb


docker run --restart=always -it -d  --name scrapydweb -p 5001:5000 -v /home/tmp:/scrapydweb chiron/scrapydweb
``` 

# 镜像升级合并
```
docker manifest rm chiron/pyspider:latest
```
```
docker manifest create -a chiron/pyspider:latest \
                        chiron/pyspider:amd64 \
                        chiron/pyspider:arm
```

## 上传信息至平台
```
docker manifest push chiron/pyspider:latest
```